package com.joao.net.domain;

public class Payment {

    private String name;
    private Double dayInCome;
    private Integer days;
    private Double total;

    public Payment() {

    }

    public Payment(String name, Double dayInCome, Integer days) {
        this.name = name;
        this.dayInCome = dayInCome;
        this.days = days;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDayInCome() {
        return dayInCome;
    }

    public void setDayInCome(Double dayInCome) {
        this.dayInCome = dayInCome;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Double getTotal() {
        this.total = this.days * this.dayInCome;
        return this.total;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "name='" + name + '\'' +
                ", dayInCome=" + dayInCome +
                ", days=" + days +
                ", total=" + total +
                '}';
    }
}
