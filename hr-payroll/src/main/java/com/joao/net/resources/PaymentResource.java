package com.joao.net.resources;


import com.joao.net.domain.Payment;
import com.joao.net.services.PaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/payments")
public class PaymentResource {

    @Autowired
    private PaymentService service;

    @HystrixCommand(fallbackMethod = "getPaymentsAlternative")
    @GetMapping(path = "/{workerId}/days/{days}")
    public ResponseEntity<Payment> getPayments(@PathVariable Long workerId, @PathVariable int days) {
        return ResponseEntity.ok(service.getNewPayment(workerId, days));
    }

    public ResponseEntity<Payment> getPaymentsAlternative(Long workerId,  int days) {
        return  ResponseEntity.ok( new Payment ("BRAM",400.0 , days) );
    }

}
