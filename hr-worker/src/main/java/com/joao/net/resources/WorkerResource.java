package com.joao.net.resources;


import com.joao.net.domain.Worker;
import com.joao.net.services.WorkerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/workers")
public class WorkerResource {

    private static Logger logger = LoggerFactory.getLogger(WorkerResource.class);

    @Autowired
    private WorkerService service;

    @Autowired
    Environment environment;

    @GetMapping
    public ResponseEntity<List<Worker>> findAll() {
        return ResponseEntity.ok(service.getAllWorkers());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Worker> findById(@PathVariable Long id) {
    	
    	if(1 == 2) {
    		throw new RuntimeException() ;
    	}


        logger.info("PORT = " + environment.getProperty("local.server.port"));

        return ResponseEntity.ok(service.getWorkersById(id));
    }
}
