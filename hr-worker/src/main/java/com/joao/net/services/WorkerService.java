package com.joao.net.services;

import com.joao.net.domain.Worker;
import com.joao.net.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkerService {

    @Autowired
    private WorkerRepository repository;

    public List<Worker> getAllWorkers() {
        return repository.findAll();
    }

    public Worker getWorkersById(Long id) {
        if (repository.findById(id).isPresent())
            return repository.findById(id).get();
        return null;
    }
}
